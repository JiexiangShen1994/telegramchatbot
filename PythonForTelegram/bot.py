
# 机器人后台信息截获：https://api.telegram.org/bot1090019615:AAGUfHESd2MRGOXY9ww8HZij3gyZ_sJcjBA/getUpdates
# 官方api环境配置：https://github.com/eternnoir/pyTelegramBotAPI#getting-started
# python解析器：conda
# 服务器用的是免费的pythonanywhere
# 任何疑问请联系：邮箱：shenjiexiang@gmail.com,telegram: join group: ChinaIndieGame and I will add you
import telebot
import time

bot_token = '1090019615:AAGUfHESd2MRGOXY9ww8HZij3gyZ_sJcjBA'

bot = telebot.TeleBot(token=bot_token)


def find_at(msg):
    for text in msg:
        if '@' in text:
            return text


@bot.message_handler(commands=['开始', 'start'])
def send_welcome(message):
    bot.reply_to(message, '欢迎使用 jay1994 bot\n/帮助\n获取帮助 ')


@bot.message_handler(commands=['帮助', 'help'])
def send_help(message):
    bot.reply_to(message, '人物搜索功能：')
    bot.reply_to(message, '键入"@"+ “ins用户名”，将显示个人资料')
    bot.reply_to(message, '电报 界面转中文：')
    bot.reply_to(message, 'https://t.me/setlanguage/zhcncc')


@bot.message_handler(func=lambda msg: msg.text is not None and '@' in msg.text)
def at_answer(message):
    texts = message.text.split()
    at_text = find_at(texts)
    bot.reply_to(message, 'https://instagram.com/{}'.format(at_text[1:]))


@bot.message_handler(func=lambda m: True)
def echo_all(message):
    bot.reply_to(message, "你发送了" + message.text)


bot.polling()

# while True:
#     try:
#         bot.polling()
#     except Exception:
#         time.sleep(15)
